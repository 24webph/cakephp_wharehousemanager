<?php
namespace WharehouseManager\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use WharehouseManager\Model\Table\MeasureUnitsTable;

/**
 * WharehouseManager\Model\Table\MeasureUnitsTable Test Case
 */
class MeasureUnitsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \WharehouseManager\Model\Table\MeasureUnitsTable
     */
    public $MeasureUnits;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.WharehouseManager.MeasureUnits',
        'plugin.WharehouseManager.Products'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('MeasureUnits') ? [] : ['className' => MeasureUnitsTable::class];
        $this->MeasureUnits = TableRegistry::getTableLocator()->get('MeasureUnits', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->MeasureUnits);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
