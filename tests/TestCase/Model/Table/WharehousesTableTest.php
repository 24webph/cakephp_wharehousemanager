<?php
namespace WharehouseManager\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use WharehouseManager\Model\Table\WharehousesTable;

/**
 * WharehouseManager\Model\Table\WharehousesTable Test Case
 */
class WharehousesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \WharehouseManager\Model\Table\WharehousesTable
     */
    public $Wharehouses;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.WharehouseManager.Wharehouses',
        'plugin.WharehouseManager.Managers',
        'plugin.WharehouseManager.WharehouseTypes',
        'plugin.WharehouseManager.Suppliers',
        'plugin.WharehouseManager.Products'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Wharehouses') ? [] : ['className' => WharehousesTable::class];
        $this->Wharehouses = TableRegistry::getTableLocator()->get('Wharehouses', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Wharehouses);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
