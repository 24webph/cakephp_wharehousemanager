<?php
namespace WharehouseManager\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use WharehouseManager\Model\Table\ProductsWharehousesTable;

/**
 * WharehouseManager\Model\Table\ProductsWharehousesTable Test Case
 */
class ProductsWharehousesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \WharehouseManager\Model\Table\ProductsWharehousesTable
     */
    public $ProductsWharehouses;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.WharehouseManager.ProductsWharehouses',
        'plugin.WharehouseManager.Products',
        'plugin.WharehouseManager.Wharehouses'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('ProductsWharehouses') ? [] : ['className' => ProductsWharehousesTable::class];
        $this->ProductsWharehouses = TableRegistry::getTableLocator()->get('ProductsWharehouses', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ProductsWharehouses);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
