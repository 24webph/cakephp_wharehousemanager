<?php
namespace WharehouseManager\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use WharehouseManager\Model\Table\WharehouseTypesTable;

/**
 * WharehouseManager\Model\Table\WharehouseTypesTable Test Case
 */
class WharehouseTypesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \WharehouseManager\Model\Table\WharehouseTypesTable
     */
    public $WharehouseTypes;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.WharehouseManager.WharehouseTypes',
        'plugin.WharehouseManager.Wharehouses'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('WharehouseTypes') ? [] : ['className' => WharehouseTypesTable::class];
        $this->WharehouseTypes = TableRegistry::getTableLocator()->get('WharehouseTypes', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->WharehouseTypes);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
