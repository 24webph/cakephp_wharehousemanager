<?php
/**
 * @var \App\View\AppView $this
 * @var \Cake\Datasource\EntityInterface $productsWharehouse
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Products Wharehouses'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Products'), ['controller' => 'Products', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Product'), ['controller' => 'Products', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Wharehouses'), ['controller' => 'Wharehouses', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Wharehouse'), ['controller' => 'Wharehouses', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="productsWharehouses form large-9 medium-8 columns content">
    <?= $this->Form->create($productsWharehouse) ?>
    <fieldset>
        <legend><?= __('Transfer Products') ?></legend>
        <?php
            echo $this->Form->control('wharehouse_id', ['options' => $wharehouses]);
            echo $this->Form->control('transfer_wharehouse_id', ['options' => $transferWharehouses]);
        ?>
        <?php foreach ($wharehouse->products as $key => $WharehouseProduct): ?>
            
                <?php
                //echo $this->Form->hidden(sprintf('product.%s.id', $key), ['value' => $WharehouseProduct->_joinData->id]);
                echo $this->Form->hidden(sprintf('product.%s.transfer_wharehouse_id', $key), ['value' => $WharehouseProduct->wharehouse_id]);
                echo $this->Form->hidden(sprintf('product.%s.wharehouse_id', $key), ['value' => $WharehouseProduct->wharehouse_id]);
                echo $this->Form->hidden(sprintf('product.%s.minimum_stock', $key), ['value' => 0]);
                echo $this->Form->hidden(sprintf('product.%s.product_id', $key), ['value' => $WharehouseProduct->id]);
                ?>
                <div class="row">
                    <?= h($WharehouseProduct->name) ?>
                    <?php echo $this->Form->control(sprintf('product.%s.transfer', $key), ['value' => $WharehouseProduct->_joinData->quantity, 'readonly' => 'readonly', 'label' => __('Stock')]); ?>
                    <?php echo $this->Form->control(sprintf('product.%s.quantity', $key), ['type' => 'number', 'placeholder' => '0.00', 'required' => 'required', 'min' => '0', 'max' => $WharehouseProduct->_joinData->quantity, 'value' => '0.00', 'step' => "0.01", 'label' => __('Transfer')]); ?>
                </div>
                <hr/>
            <?php endforeach; ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
