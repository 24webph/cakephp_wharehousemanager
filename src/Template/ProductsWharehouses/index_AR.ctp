<?php
/**
 * @var \App\View\AppView $this
 * @var \Cake\Datasource\EntityInterface[]|\Cake\Collection\CollectionInterface $productsWharehouses
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Products Wharehouse'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Products'), ['controller' => 'Products', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Product'), ['controller' => 'Products', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Wharehouses'), ['controller' => 'Wharehouses', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Wharehouse'), ['controller' => 'Wharehouses', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="productsWharehouses index large-9 medium-8 columns content">
    <h3><?= __('Products Wharehouses') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('product_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('wharehouse_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('quantity') ?></th>
                <th scope="col"><?= $this->Paginator->sort('minimum_stock') ?></th>
                <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                <th scope="col"><?= $this->Paginator->sort('modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($productsWharehouses as $productsWharehouse): ?>
            <tr>
                <td><?= $this->Number->format($productsWharehouse->id) ?></td>
                <td><?= $productsWharehouse->has('product') ? $this->Html->link($productsWharehouse->product->name, ['controller' => 'Products', 'action' => 'view', $productsWharehouse->product->id]) : '' ?></td>
                <td><?= $productsWharehouse->has('wharehouse') ? $this->Html->link($productsWharehouse->wharehouse->name, ['controller' => 'Wharehouses', 'action' => 'view', $productsWharehouse->wharehouse->id]) : '' ?></td>
                <td><?= $this->Number->format($productsWharehouse->quantity) ?></td>
                <td><?= $this->Number->format($productsWharehouse->minimum_stock) ?></td>
                <td><?= h($productsWharehouse->created) ?></td>
                <td><?= h($productsWharehouse->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $productsWharehouse->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $productsWharehouse->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $productsWharehouse->id], ['confirm' => __('Are you sure you want to delete # {0}?', $productsWharehouse->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
