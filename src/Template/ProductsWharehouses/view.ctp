<?php
/**
 * @var \App\View\AppView $this
 * @var \Cake\Datasource\EntityInterface $productsWharehouse
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Products Wharehouse'), ['action' => 'edit', $productsWharehouse->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Products Wharehouse'), ['action' => 'delete', $productsWharehouse->id], ['confirm' => __('Are you sure you want to delete # {0}?', $productsWharehouse->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Products'), ['controller' => 'Products', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Product'), ['controller' => 'Products', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Wharehouses'), ['controller' => 'Wharehouses', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Wharehouse'), ['controller' => 'Wharehouses', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="productsWharehouses view large-9 medium-8 columns content">
    <h3><?= h($productsWharehouse->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Product') ?></th>
            <td><?= $productsWharehouse->has('product') ? $this->Html->link($productsWharehouse->product->name, ['controller' => 'Products', 'action' => 'view', $productsWharehouse->product->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Wharehouse') ?></th>
            <td><?= $productsWharehouse->has('wharehouse') ? $this->Html->link($productsWharehouse->wharehouse->name, ['controller' => 'Wharehouses', 'action' => 'view', $productsWharehouse->wharehouse->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($productsWharehouse->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Quantity') ?></th>
            <td><?= $this->Number->format($productsWharehouse->quantity) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Minimum Stock') ?></th>
            <td><?= $this->Number->format($productsWharehouse->minimum_stock) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($productsWharehouse->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($productsWharehouse->modified) ?></td>
        </tr>
    </table>
</div>
