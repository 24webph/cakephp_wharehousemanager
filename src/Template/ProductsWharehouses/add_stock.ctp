<?php
/**
 * @var \App\View\AppView $this
 * @var \Cake\Datasource\EntityInterface $productsWharehouse
 */
?>
<?php
$this->Html->css([
    'template/blue.css',
    'template/switchery.min.css'], ['block' => true]);
?>
<div class="col-md-12 col-sm-12 col-xs-12  text-center">
    <div class="x_panel">
        <div class="x_title">
            <h2><?php echo $wharehouse->name; ?> - Produto: <small>Adicionar Stock</small></h2>
            <div class="filter">
                <div class="title_right">
                    <div class="col-md-8 col-sm-8 col-xs-12 form-group pull-right top_search">
                        <div class="btn-group" id="listar">
                            <?php $tagI = $this->Html->tag('I', '', ['class' => 'fa fa-table']); ?>
                            <?= $this->Html->link($tagI . ' ' . 'Listar', ['controller' => 'Wharehouses', 'action' => 'edit', $wharehouse->id], ['class' => 'btn btn-primary btn-sm', 'type' => 'button', 'aria-expanded' => 'false', 'escape' => false]) ?>
                        </div>

                    </div>
                </div>
            </div>


            <div class="clearfix"></div>
        </div>
        <div class="x_content">

            <div class="row"> 

                <!-- /.col --> 
                <div class="col-md-12">
                    <div class="" role="tabpanel" data-example-id="togglable-tabs">
                        <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                            <li role="presentation" class="active"><a href="#details" id="details-tab" role="tab" data-toggle="tab" aria-expanded="true">Ficha</a>
                            </li>

                        </ul>
                        <div id="myTabContent" class="tab-content">
                            <div role="tabpanel" class="tab-pane fade active in" id="details" aria-labelledby="home-tab">
                                <?= $this->Form->create($productsWharehouse, ['class' => 'form-horizontal form-label-left input_mask']) ?>
                                <div class="item form-group">
                                    <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback">
                                        <?php echo $this->Form->control('wharehouse_id', ['options' => $wharehouses, 'label' => 'Armazem', 'class' => 'form-control col-md-7 col-xs-12 has-feedback-left', 'placeholder' => 'Armazem']); ?>
                                        <span class="fa fa-product-hunt form-control-feedback left" aria-hidden="true"></span>
                                    </div>
                                    <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback">
                                        <?php echo $this->Form->control('product_id', ['options' => $products, 'label' => 'Produto', 'class' => 'form-control col-md-7 col-xs-12 has-feedback-left', 'placeholder' => 'Produto']); ?>
                                        <span class="fa fa-product-hunt form-control-feedback left" aria-hidden="true"></span>
                                    </div>

                                    <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback">
                                        <?php echo $this->Form->control('quantity', ['label' => 'Quantidade', 'class' => 'form-control col-md-7 col-xs-12 has-feedback-left', 'placeholder' => 'Quantidade']); ?>
                                        <span class="fa fa-star form-control-feedback left" aria-hidden="true"></span>
                                    </div>
                                </div>
                                <div class="item form-group hidden">
                                     <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback">
                                        <?php echo $this->Form->control('minimum_stock', ['label' => 'Stock Minimo', 'class' => 'form-control col-md-7 col-xs-12 has-feedback-left', 'placeholder' => 'Stock Minimo']); ?>
                                        <span class="fa fa-star form-control-feedback left" aria-hidden="true"></span>
                                    </div>
                                </div>
                                <div class="col-md-12 col-sm-12 col-xs-12 text-right">
                                    <?= $this->Form->button('Registar', ['class' => 'btn btn-primary']) ?>
                                    <?= $this->Html->link('Cancelar', ['action' => 'index'], ['class' => 'btn btn-warning', 'confirm' => __('Tem a certeza que quer cancelar?'), 'escape' => false]) ?>

                                </div>


                                <?= $this->Form->end() ?>                    


                            </div>
                        </div>
                    </div>      
                </div>
                <!-- /.col -->
            </div>

        </div>
    </div>
</div>

<?php
$this->Html->script([
    'template/icheck.min.js',
    'template/switchery.min.js',
    'template/jquery.inputmask.bundle.js',
    'template/add_edit.js'], ['block' => true]);
?>




