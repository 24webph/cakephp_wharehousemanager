<?php
/**
 * @var \App\View\AppView $this
 * @var \Cake\Datasource\EntityInterface[]|\Cake\Collection\CollectionInterface $wharehouses
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Wharehouse'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Businesses'), ['controller' => 'Businesses', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Business'), ['controller' => 'Businesses', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Managers'), ['controller' => 'Employees', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Manager'), ['controller' => 'Employees', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Wharehouse Types'), ['controller' => 'WharehouseTypes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Wharehouse Type'), ['controller' => 'WharehouseTypes', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Products'), ['controller' => 'Products', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Product'), ['controller' => 'Products', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="wharehouses index large-9 medium-8 columns content">
    <h3><?= __('Wharehouses') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('name') ?></th>
                <th scope="col"><?= $this->Paginator->sort('manager_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('business_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('wharehouse_type_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('active') ?></th>
                <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                <th scope="col"><?= $this->Paginator->sort('modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($wharehouses as $wharehouse): ?>
            <tr>
                <td><?= $this->Number->format($wharehouse->id) ?></td>
                <td><?= h($wharehouse->name) ?></td>
                <td><?= $wharehouse->has('manager') ? $this->Html->link($wharehouse->manager->name, ['controller' => 'Employees', 'action' => 'view', $wharehouse->manager->id]) : '' ?></td>
                <td><?= $wharehouse->has('business') ? $this->Html->link($wharehouse->business->name, ['controller' => 'Businesses', 'action' => 'view', $wharehouse->business->id]) : '' ?></td>
                <td><?= $wharehouse->has('wharehouse_type') ? $this->Html->link($wharehouse->wharehouse_type->name, ['controller' => 'WharehouseTypes', 'action' => 'view', $wharehouse->wharehouse_type->id]) : '' ?></td>
                <td><?= h($wharehouse->active) ?></td>
                <td><?= h($wharehouse->created) ?></td>
                <td><?= h($wharehouse->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $wharehouse->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $wharehouse->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $wharehouse->id], ['confirm' => __('Are you sure you want to delete # {0}?', $wharehouse->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
