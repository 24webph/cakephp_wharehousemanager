<?php
/**
 * @var \App\View\AppView $this
 * @var \Cake\Datasource\EntityInterface $wharehouse
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Wharehouse'), ['action' => 'edit', $wharehouse->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Wharehouse'), ['action' => 'delete', $wharehouse->id], ['confirm' => __('Are you sure you want to delete # {0}?', $wharehouse->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Wharehouses'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Wharehouse'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Businesses'), ['controller' => 'Businesses', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Business'), ['controller' => 'Businesses', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Managers'), ['controller' => 'Employees', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Manager'), ['controller' => 'Employees', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Wharehouse Types'), ['controller' => 'WharehouseTypes', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Wharehouse Type'), ['controller' => 'WharehouseTypes', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Products'), ['controller' => 'Products', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Product'), ['controller' => 'Products', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('Add Product'), ['controller' => 'ProductsWharehouses', 'action' => 'add',$wharehouse->id]) ?> </li>
        <li><?= $this->Html->link(__('Transfer Product'), ['controller' => 'ProductsWharehouses', 'action' => 'transfer',$wharehouse->id]) ?> </li>
    </ul>
</nav>
<div class="wharehouses view large-9 medium-8 columns content">
    <h3><?= h($wharehouse->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Name') ?></th>
            <td><?= h($wharehouse->name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Manager') ?></th>
            <td><?= $wharehouse->has('manager') ? $this->Html->link($wharehouse->manager->name, ['controller' => 'Employees', 'action' => 'view', $wharehouse->manager->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Business') ?></th>
            <td><?= $wharehouse->has('business') ? $this->Html->link($wharehouse->business->name, ['controller' => 'Businesses', 'action' => 'view', $wharehouse->business->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Wharehouse Type') ?></th>
            <td><?= $wharehouse->has('wharehouse_type') ? $this->Html->link($wharehouse->wharehouse_type->name, ['controller' => 'WharehouseTypes', 'action' => 'view', $wharehouse->wharehouse_type->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($wharehouse->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($wharehouse->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($wharehouse->modified) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Active') ?></th>
            <td><?= $wharehouse->active ? __('Yes') : __('No'); ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Products') ?></h4>
        <?php if (!empty($wharehouse->products)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Name') ?></th>
                <th scope="col"><?= __('Part Number') ?></th>
                <th scope="col"><?= __('Label') ?></th>
                <th scope="col"><?= __('Price') ?></th>
                <th scope="col"><?= __('Stock') ?></th>
                <th scope="col"><?= __('Active') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col"><?= __('Modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($wharehouse->products as $products): ?>
            <tr>
                <td><?= h($products->id) ?></td>
                <td><?= h($products->name) ?></td>
                <td><?= h($products->part_number) ?></td>
                <td><?= h($products->label) ?></td>
                <td><?= h($products->price) ?></td>
                <td><?= h($products->_joinData->quantity) ?></td>
                <td><?= h($products->active) ?></td>
                <td><?= h($products->created) ?></td>
                <td><?= h($products->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Products', 'action' => 'view', $products->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'ProductsWharehouses', 'action' => 'edit', $products->_joinData->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'ProductsWharehouses', 'action' => 'delete', $products->_joinData->id], ['confirm' => __('Are you sure you want to remove # {0}?', $products->name)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
