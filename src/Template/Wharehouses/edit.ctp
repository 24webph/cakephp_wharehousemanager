<?php
/**
 * @var \App\View\AppView $this
 * @var \Cake\Datasource\EntityInterface $wharehouse
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $wharehouse->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $wharehouse->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Wharehouses'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Businesses'), ['controller' => 'Businesses', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Business'), ['controller' => 'Businesses', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Managers'), ['controller' => 'Employees', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Manager'), ['controller' => 'Employees', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Wharehouse Types'), ['controller' => 'WharehouseTypes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Wharehouse Type'), ['controller' => 'WharehouseTypes', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Products'), ['controller' => 'Products', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Product'), ['controller' => 'Products', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="wharehouses form large-9 medium-8 columns content">
    <?= $this->Form->create($wharehouse) ?>
    <fieldset>
        <legend><?= __('Edit Wharehouse') ?></legend>
        <?php
            echo $this->Form->control('name');
            echo $this->Form->control('manager_id', ['options' => $managers, 'empty' => true]);
            echo $this->Form->control('business_id', ['options' => $businesses, 'empty' => true]);
            echo $this->Form->control('wharehouse_type_id', ['options' => $wharehouseTypes]);
            echo $this->Form->control('active');
            //echo $this->Form->control('products._ids', ['options' => $products]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
