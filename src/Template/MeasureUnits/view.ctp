<?php
/**
 * @var \App\View\AppView $this
 * @var \Cake\Datasource\EntityInterface $measureUnit
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Measure Unit'), ['action' => 'edit', $measureUnit->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Measure Unit'), ['action' => 'delete', $measureUnit->id], ['confirm' => __('Are you sure you want to delete # {0}?', $measureUnit->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Measure Units'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Measure Unit'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Products'), ['controller' => 'Products', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Product'), ['controller' => 'Products', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="measureUnits view large-9 medium-8 columns content">
    <h3><?= h($measureUnit->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Name') ?></th>
            <td><?= h($measureUnit->name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Abbreviation') ?></th>
            <td><?= h($measureUnit->abbreviation) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($measureUnit->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($measureUnit->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($measureUnit->modified) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Active') ?></th>
            <td><?= $measureUnit->active ? __('Yes') : __('No'); ?></td>
        </tr>
    </table>
   
</div>
