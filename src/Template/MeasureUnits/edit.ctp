<?php
/**
 * @var \App\View\AppView $this
 * @var \Cake\Datasource\EntityInterface $measureUnit
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $measureUnit->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $measureUnit->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Measure Units'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Products'), ['controller' => 'Products', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Product'), ['controller' => 'Products', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="measureUnits form large-9 medium-8 columns content">
    <?= $this->Form->create($measureUnit) ?>
    <fieldset>
        <legend><?= __('Edit Measure Unit') ?></legend>
        <?php
            echo $this->Form->control('name');
            echo $this->Form->control('abbreviation');
            echo $this->Form->control('active');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
