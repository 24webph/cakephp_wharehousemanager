<?php
/**
 * @var \App\View\AppView $this
 * @var \Cake\Datasource\EntityInterface $wharehouseType
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $wharehouseType->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $wharehouseType->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Wharehouse Types'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Wharehouses'), ['controller' => 'Wharehouses', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Wharehouse'), ['controller' => 'Wharehouses', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="wharehouseTypes form large-9 medium-8 columns content">
    <?= $this->Form->create($wharehouseType) ?>
    <fieldset>
        <legend><?= __('Edit Wharehouse Type') ?></legend>
        <?php
            echo $this->Form->control('name');
            echo $this->Form->control('active');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
