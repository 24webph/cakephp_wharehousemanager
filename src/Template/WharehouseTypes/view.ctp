<?php
/**
 * @var \App\View\AppView $this
 * @var \Cake\Datasource\EntityInterface $wharehouseType
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Wharehouse Type'), ['action' => 'edit', $wharehouseType->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Wharehouse Type'), ['action' => 'delete', $wharehouseType->id], ['confirm' => __('Are you sure you want to delete # {0}?', $wharehouseType->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Wharehouse Types'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Wharehouse Type'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Wharehouses'), ['controller' => 'Wharehouses', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Wharehouse'), ['controller' => 'Wharehouses', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="wharehouseTypes view large-9 medium-8 columns content">
    <h3><?= h($wharehouseType->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Name') ?></th>
            <td><?= h($wharehouseType->name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($wharehouseType->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($wharehouseType->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($wharehouseType->modified) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Active') ?></th>
            <td><?= $wharehouseType->active ? __('Yes') : __('No'); ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Wharehouses') ?></h4>
        <?php if (!empty($wharehouseType->wharehouses)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Name') ?></th>
                <th scope="col"><?= __('Active') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col"><?= __('Modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($wharehouseType->wharehouses as $wharehouses): ?>
            <tr>
                <td><?= h($wharehouses->id) ?></td>
                <td><?= h($wharehouses->name) ?></td>
                <td><?= $wharehouses->active ? __('Yes') : __('No'); ?></td>
                <td><?= h($wharehouses->created) ?></td>
                <td><?= h($wharehouses->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Wharehouses', 'action' => 'view', $wharehouses->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Wharehouses', 'action' => 'edit', $wharehouses->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Wharehouses', 'action' => 'delete', $wharehouses->id], ['confirm' => __('Are you sure you want to delete # {0}?', $wharehouses->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
