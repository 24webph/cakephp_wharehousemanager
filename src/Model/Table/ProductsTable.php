<?php

namespace WharehouseManager\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Products Model
 *
 * @property \WharehouseManager\Model\Table\SuppliersTable|\Cake\ORM\Association\BelongsTo $Suppliers
 * @property \WharehouseManager\Model\Table\ProductCategoriesTable|\Cake\ORM\Association\BelongsTo $ProductCategories
 * @property \WharehouseManager\Model\Table\MeasureUnitsTable|\Cake\ORM\Association\BelongsTo $MeasureUnits
 * @property \WharehouseManager\Model\Table\WharehousesTable|\Cake\ORM\Association\BelongsToMany $Wharehouses
 *
 * @method \WharehouseManager\Model\Entity\Product get($primaryKey, $options = [])
 * @method \WharehouseManager\Model\Entity\Product newEntity($data = null, array $options = [])
 * @method \WharehouseManager\Model\Entity\Product[] newEntities(array $data, array $options = [])
 * @method \WharehouseManager\Model\Entity\Product|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \WharehouseManager\Model\Entity\Product saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \WharehouseManager\Model\Entity\Product patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \WharehouseManager\Model\Entity\Product[] patchEntities($entities, array $data, array $options = [])
 * @method \WharehouseManager\Model\Entity\Product findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class ProductsTable extends Table {

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config) {
        parent::initialize($config);

        $this->setTable('products');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Suppliers', [
            'foreignKey' => 'supplier_id',
            'className' => 'WharehouseManager.Suppliers'
        ]);
        $this->belongsTo('ProductCategories', [
            'foreignKey' => 'product_category_id',
            'joinType' => 'INNER',
            'className' => 'WharehouseManager.ProductCategories'
        ]);
        $this->belongsTo('MeasureUnits', [
            'foreignKey' => 'measure_unit_id',
            'joinType' => 'INNER',
            'className' => 'WharehouseManager.MeasureUnits'
        ]);
        
        $this->belongsToMany('Courses', [
            'through' => 'CoursesMemberships',
        ]);
        $this->belongsToMany('Wharehouses', [
            'through' => 'WharehouseManager.ProductsWharehouses',
            'foreignKey' => 'product_id',
            'targetForeignKey' => 'wharehouse_id',
            'joinTable' => 'products_wharehouses',
            'className' => 'WharehouseManager.Wharehouses'
        ]);

       
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator) {
        $validator
                ->integer('id')
                ->allowEmptyString('id', 'create');

        $validator
                ->scalar('name')
                ->maxLength('name', 100)
                ->requirePresence('name', 'create')
                ->allowEmptyString('name', false);

        $validator
                ->scalar('part_number')
                ->maxLength('part_number', 64)
                ->requirePresence('part_number', 'create')
                ->allowEmptyString('part_number', false);

        $validator
                ->scalar('label')
                ->maxLength('label', 100)
                ->allowEmptyString('label');

        $validator
                ->decimal('price')
                ->allowEmptyString('price', false);

        $validator
                ->boolean('active')
                ->allowEmptyString('active');

        $validator
                ->integer('product_category_id')
                ->notEmpty('product_category_id');

        $validator
                ->integer('measure_unit_id')
                ->notEmpty('measure_unit_id');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules) {
        $rules->add($rules->existsIn(['supplier_id'], 'Suppliers'));
        $rules->add($rules->existsIn(['product_category_id'], 'ProductCategories'));
        $rules->add($rules->existsIn(['measure_unit_id'], 'MeasureUnits'));

        return $rules;
    }

}
