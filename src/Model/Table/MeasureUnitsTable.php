<?php
namespace WharehouseManager\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * MeasureUnits Model
 *
 * @property \WharehouseManager\Model\Table\ProductsTable|\Cake\ORM\Association\HasMany $Products
 *
 * @method \WharehouseManager\Model\Entity\MeasureUnit get($primaryKey, $options = [])
 * @method \WharehouseManager\Model\Entity\MeasureUnit newEntity($data = null, array $options = [])
 * @method \WharehouseManager\Model\Entity\MeasureUnit[] newEntities(array $data, array $options = [])
 * @method \WharehouseManager\Model\Entity\MeasureUnit|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \WharehouseManager\Model\Entity\MeasureUnit saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \WharehouseManager\Model\Entity\MeasureUnit patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \WharehouseManager\Model\Entity\MeasureUnit[] patchEntities($entities, array $data, array $options = [])
 * @method \WharehouseManager\Model\Entity\MeasureUnit findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class MeasureUnitsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('measure_units');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->hasMany('Products', [
            'foreignKey' => 'measure_unit_id',
            'className' => 'WharehouseManager.Products'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 100)
            ->requirePresence('name', 'create')
            ->allowEmptyString('name', false);

        $validator
            ->scalar('abbreviation')
            ->maxLength('abbreviation', 5)
            ->requirePresence('abbreviation', 'create')
            ->allowEmptyString('abbreviation', false);

        $validator
            ->boolean('active')
            ->allowEmptyString('active');

        return $validator;
    }
}
