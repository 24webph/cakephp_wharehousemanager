<?php
namespace WharehouseManager\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Wharehouses Model
 *
 * @property \BusinessManager\Model\Table\EmployeesTable|\Cake\ORM\Association\BelongsTo $Managers
 * @property \WharehouseManager\Model\Table\WharehouseTypesTable|\Cake\ORM\Association\BelongsTo $WharehouseTypes
 * @property \WharehouseManager\Model\Table\ProductsTable|\Cake\ORM\Association\BelongsToMany $Products
 *
 * @method \WharehouseManager\Model\Entity\Wharehouse get($primaryKey, $options = [])
 * @method \WharehouseManager\Model\Entity\Wharehouse newEntity($data = null, array $options = [])
 * @method \WharehouseManager\Model\Entity\Wharehouse[] newEntities(array $data, array $options = [])
 * @method \WharehouseManager\Model\Entity\Wharehouse|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \WharehouseManager\Model\Entity\Wharehouse saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \WharehouseManager\Model\Entity\Wharehouse patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \WharehouseManager\Model\Entity\Wharehouse[] patchEntities($entities, array $data, array $options = [])
 * @method \WharehouseManager\Model\Entity\Wharehouse findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class WharehousesTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('wharehouses');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
        $this->addBehavior('BusinessManager.Business');
        
        $this->belongsTo('WharehouseTypes', [
            'foreignKey' => 'wharehouse_type_id',
            'joinType' => 'INNER',
            'className' => 'WharehouseManager.WharehouseTypes'
        ]);
        
        $this->belongsToMany('Products', [
            'through' => 'WharehouseManager.ProductsWharehouses',
            'foreignKey' => 'wharehouse_id',
            'targetForeignKey' => 'product_id',
            'joinTable' => 'products_wharehouses',
            'className' => 'WharehouseManager.Products'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 100)
            ->requirePresence('name', 'create')
            ->allowEmptyString('name', false);

        $validator
            ->boolean('active')
            ->allowEmptyString('active');

        $validator
            ->integer('wharehouse_type_id')
            ->notEmpty('wharehouse_type_id');
        
        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['wharehouse_type_id'], 'WharehouseTypes'));
        return $rules;
    }
}
