<?php

namespace WharehouseManager\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use WharehouseManager\Model\Entity\ProductsWharehouse;

/**
 * ProductsWharehouses Model
 *
 * @property \WharehouseManager\Model\Table\ProductsTable|\Cake\ORM\Association\BelongsTo $Products
 * @property \WharehouseManager\Model\Table\WharehousesTable|\Cake\ORM\Association\BelongsTo $Wharehouses
 *
 * @method \WharehouseManager\Model\Entity\ProductsWharehouse get($primaryKey, $options = [])
 * @method \WharehouseManager\Model\Entity\ProductsWharehouse newEntity($data = null, array $options = [])
 * @method \WharehouseManager\Model\Entity\ProductsWharehouse[] newEntities(array $data, array $options = [])
 * @method \WharehouseManager\Model\Entity\ProductsWharehouse|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \WharehouseManager\Model\Entity\ProductsWharehouse saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \WharehouseManager\Model\Entity\ProductsWharehouse patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \WharehouseManager\Model\Entity\ProductsWharehouse[] patchEntities($entities, array $data, array $options = [])
 * @method \WharehouseManager\Model\Entity\ProductsWharehouse findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class ProductsWharehousesTable extends Table {

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config) {
        parent::initialize($config);

        $this->setTable('products_wharehouses');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Products', [
            'foreignKey' => 'product_id',
            'joinType' => 'INNER',
            'className' => 'WharehouseManager.Products'
        ]);
        $this->belongsTo('Wharehouses', [
            'foreignKey' => 'wharehouse_id',
            'joinType' => 'INNER',
            'className' => 'WharehouseManager.Wharehouses'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator) {
        $validator
                ->integer('id')
                ->allowEmptyString('id', 'create');

        $validator
                ->decimal('quantity')
                ->requirePresence('quantity', 'create')
                ->allowEmptyString('quantity', false);

        $validator
                ->integer('minimum_stock')
                ->allowEmptyString('minimum_stock');

        $validator
                ->integer('product_id')
                ->notEmpty('product_id');

        $validator
                ->integer('wharehouse_id')
                ->notEmpty('wharehouse_id');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules) {
        $rules->add($rules->existsIn(['product_id'], 'Products'));
        $rules->add($rules->existsIn(['wharehouse_id'], 'Wharehouses'));
        $rules->add($rules->existsIn(['transfer_wharehouse_id'], 'Wharehouses'));

        return $rules;
    }

    public function addStock(ProductsWharehouse $entity) {
        if ($entity->getErrors() || $entity->quantity < 0) {
            return false;
        }
        if ($this->exists(['wharehouse_id' => $entity->wharehouse_id, 'product_id' => $entity->product_id])) {
            $product = $this->find()->where(['wharehouse_id' => $entity->wharehouse_id, 'product_id' => $entity->product_id])->first();
            $quantity = ($entity->quantity + $product->quantity);
            $product->set('quantity', $quantity);
        } else {
            $product = $entity;
        }
        return $this->save($product);
    }

    public function editStock(ProductsWharehouse $entity) {
        if ($entity->getErrors() || $entity->quantity < 0) {
            return false;
        }
        return $this->save($entity);
    }

    public function transfer(ProductsWharehouse $entity) {

        if ($entity->getErrors()) {
            return false;
        }
        if ($entity->isNew() === false) {
            return $entity;
        }

        if ($entity->quantity < 1) {
            return $entity;
        }

        $origin_wharehouse = $this->find()->where(['quantity >' => 0, 'wharehouse_id' => $entity->wharehouse_id, 'product_id' => $entity->product_id])->firstOrFail();

        if ($this->exists(['wharehouse_id' => $entity->transfer_wharehouse_id, 'product_id' => $entity->product_id])) {
            $transfer_wharehouse = $this->find()->where(['wharehouse_id' => $entity->transfer_wharehouse_id, 'product_id' => $entity->product_id])->first();
        } else {
            $transfer_wharehouse = $this->cloneFromOrigin($origin_wharehouse, $entity);
        }
        $this->updateStockOrigin($origin_wharehouse, $entity);
        $this->updateStockTransfer($transfer_wharehouse, $entity);
        return $this->saveMany([$origin_wharehouse, $transfer_wharehouse]);
    }

    public function transferMany($products) {
        $transationResult = $this->getConnection()->transactional(function () use ($products) {
            $result = true;
            foreach ($products as $entity) {
                $result = $result && $this->transfer($entity);
            }
            return $result;
        });

        return $transationResult;
    }

    private function cloneFromOrigin(ProductsWharehouse $origin_wharehouse, ProductsWharehouse $entity) {
        $transfer_wharehouse = $this->newEntity();
        $data = $origin_wharehouse->toArray();
        unset($data['id']);
        unset($data['quantity']);
        unset($data['minimun_stock']);
        $transfer_wharehouse = $this->patchEntity($transfer_wharehouse, $data);
        $transfer_wharehouse->set('wharehouse_id', $entity->transfer_wharehouse_id);
        return $transfer_wharehouse;
    }

    private function updateStockOrigin(ProductsWharehouse $origin_wharehouse, ProductsWharehouse $entity) {
        $quantity = $origin_wharehouse->quantity;
        $quantity = ($quantity - $entity->quantity);
        if ($quantity < 0) {
            $entity->set('quantity', $quantity + $entity->quantity);
            $quantity = 0;
        }
        $origin_wharehouse->set('quantity', $quantity);
        //return $origin_wharehouse;
    }

    private function updateStockTransfer(ProductsWharehouse $transfer_wharehouse, ProductsWharehouse $entity) {
        $quantity = $transfer_wharehouse->quantity;
        $quantity = ($quantity + $entity->quantity);
        $transfer_wharehouse->set('quantity', $quantity);
        //return $transfer_wharehouse;
    }

    public function findListProductTransfer(Query $query, array $options) {
        $options = array_merge($options, [
            'keyField' => 'product_id',
            'valueField' => 'product.name'
        ]);
        if (isset($options['wharehouse_id'])) {
            $query->where(['wharehouse_id' => $options['wharehouse_id']]);
        }
        $query->where(['quantity >' => 0])->contain(['Products']);
        return $this->findList($query, $options);
    }

}
