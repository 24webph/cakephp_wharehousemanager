<?php
namespace WharehouseManager\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * WharehouseTypes Model
 *
 * @property \WharehouseManager\Model\Table\WharehousesTable|\Cake\ORM\Association\HasMany $Wharehouses
 *
 * @method \WharehouseManager\Model\Entity\WharehouseType get($primaryKey, $options = [])
 * @method \WharehouseManager\Model\Entity\WharehouseType newEntity($data = null, array $options = [])
 * @method \WharehouseManager\Model\Entity\WharehouseType[] newEntities(array $data, array $options = [])
 * @method \WharehouseManager\Model\Entity\WharehouseType|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \WharehouseManager\Model\Entity\WharehouseType saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \WharehouseManager\Model\Entity\WharehouseType patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \WharehouseManager\Model\Entity\WharehouseType[] patchEntities($entities, array $data, array $options = [])
 * @method \WharehouseManager\Model\Entity\WharehouseType findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class WharehouseTypesTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('wharehouse_types');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->hasMany('Wharehouses', [
            'foreignKey' => 'wharehouse_type_id',
            'className' => 'WharehouseManager.Wharehouses'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 100)
            ->requirePresence('name', 'create')
            ->allowEmptyString('name', false);

        $validator
            ->boolean('active')
            ->allowEmptyString('active');

        return $validator;
    }
}
