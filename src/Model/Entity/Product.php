<?php
namespace WharehouseManager\Model\Entity;

use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;
use Cake\Database\Expression\QueryExpression;
use Cake\ORM\Query;

/**
 * Product Entity
 *
 * @property int $id
 * @property string $name
 * @property int|null $supplier_id
 * @property int $product_category_id
 * @property string $part_number
 * @property string|null $label
 * @property int $measure_unit_id
 * @property float $price
 * @property bool|null $active
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $modified
 *
 * @property \WharehouseManager\Model\Entity\Supplier $supplier
 * @property \WharehouseManager\Model\Entity\ProductCategory $product_category
 * @property \WharehouseManager\Model\Entity\MeasureUnit $measure_unit
 * @property \WharehouseManager\Model\Entity\Wharehouse[] $wharehouses
 */
class Product extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'name' => true,
        'supplier_id' => true,
        'product_category_id' => true,
        'part_number' => true,
        'label' => true,
        'measure_unit_id' => true,
        'price' => true,
        'active' => true,
        'created' => true,
        'modified' => true,
        'supplier' => true,
        'product_category' => true,
        'measure_unit' => true,
        'wharehouses' => true
    ];
    
    // total stock virtual field
    protected function _getStock() {
        $stock = 0;
        $stock = $this->consumablesCounter();
        return $stock;
    }

    private function consumablesCounter() {
        $stockTable = TableRegistry::get('ProductsWharehouses');
        $query = $stockTable->find()->where(['ProductsWharehouses.product_id' => $this->_properties['id']]);
        $sum = $query->select(['sumOfProducts' => $query->func()->sum('quantity')])->first();
        return $sum->sumOfProducts;
    }
}
