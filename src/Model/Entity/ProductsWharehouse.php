<?php
namespace WharehouseManager\Model\Entity;

use Cake\ORM\Entity;

/**
 * ProductsWharehouse Entity
 *
 * @property int $id
 * @property int $product_id
 * @property int $wharehouse_id
 * @property int $transfer_wharehouse_id
 * @property float $quantity
 * @property int|null $minimum_stock
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $modified
 *
 * @property \WharehouseManager\Model\Entity\Product $product
 * @property \WharehouseManager\Model\Entity\Wharehouse $wharehouse
 */
class ProductsWharehouse extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'product_id' => true,
        'wharehouse_id' => true,
        'transfer_wharehouse_id' => true,
        'quantity' => true,
        'minimum_stock' => true,
        'created' => true,
        'modified' => true,
        'product' => true,
        'wharehouse' => true
    ];
}
