<?php
namespace WharehouseManager\Model\Entity;

use Cake\ORM\Entity;

/**
 * Wharehouse Entity
 *
 * @property int $id
 * @property string $name
 * @property int|null $manager_id
 * @property int|null $business_id
 * @property int $wharehouse_type_id
 * @property bool|null $active
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $modified
 *
 * @property \BusinessManager\Model\Entity\Employee $manager
 * @property \WharehouseManager\Model\Entity\WharehouseType $wharehouse_type
 * @property \WharehouseManager\Model\Entity\Product[] $products
 */
class Wharehouse extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'name' => true,
        'manager_id' => true,
        'business_id' => true,
        'wharehouse_type_id' => true,
        'active' => true,
        'created' => true,
        'modified' => true,
        'manager' => true,
        'business' => true,
        'wharehouse_type' => true,
        'products' => true
    ];
}
