<?php
namespace WharehouseManager\Model\Entity;

use Cake\ORM\Entity;

/**
 * MeasureUnit Entity
 *
 * @property int $id
 * @property string $name
 * @property string $abbreviation
 * @property bool|null $active
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $modified
 *
 * @property \WharehouseManager\Model\Entity\Product[] $products
 */
class MeasureUnit extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'name' => true,
        'abbreviation' => true,
        'active' => true,
        'created' => true,
        'modified' => true,
        'products' => true
    ];
}
