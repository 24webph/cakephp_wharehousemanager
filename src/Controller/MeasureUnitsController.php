<?php
namespace WharehouseManager\Controller;

use WharehouseManager\Controller\AppController;

/**
 * MeasureUnits Controller
 *
 * @property \WharehouseManager\Model\Table\MeasureUnitsTable $MeasureUnits
 *
 * @method \WharehouseManager\Model\Entity\MeasureUnit[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class MeasureUnitsController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $measureUnits = $this->paginate($this->MeasureUnits);

        $this->set(compact('measureUnits'));
    }

    /**
     * View method
     *
     * @param string|null $id Measure Unit id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $measureUnit = $this->MeasureUnits->get($id, [
            'contain' => []
        ]);

        $this->set('measureUnit', $measureUnit);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $measureUnit = $this->MeasureUnits->newEntity();
        if ($this->request->is('post')) {
            $measureUnit = $this->MeasureUnits->patchEntity($measureUnit, $this->request->getData());
            if ($this->MeasureUnits->save($measureUnit)) {
                $this->Flash->success(__('Saved with success.'));

                return $this->redirect(['action' => 'edit',$measureUnit->id]);
            }
            $this->Flash->error(__('Could not be saved. Please, try again.'));
        }
        $this->set(compact('measureUnit'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Measure Unit id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $measureUnit = $this->MeasureUnits->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $measureUnit = $this->MeasureUnits->patchEntity($measureUnit, $this->request->getData());
            if ($this->MeasureUnits->save($measureUnit)) {
                $this->Flash->success(__('Saved with success.'));
                return $this->redirect(['action' => 'edit',$id]);
            }
            $this->Flash->error(__('Could not be saved. Please, try again.'));
        }
        $this->set(compact('measureUnit'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Measure Unit id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $measureUnit = $this->MeasureUnits->get($id);
        if ($this->MeasureUnits->delete($measureUnit)) {
            $this->Flash->success(__('Deleted with success.'));
        } else {
            $this->Flash->error(__('Could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
