<?php
namespace WharehouseManager\Controller;

use WharehouseManager\Controller\AppController;

/**
 * Wharehouses Controller
 *
 * @property \WharehouseManager\Model\Table\WharehousesTable $Wharehouses
 *
 * @method \WharehouseManager\Model\Entity\Wharehouse[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class WharehousesController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Businesses', 'Managers', 'WharehouseTypes']
        ];
        $wharehouses = $this->paginate($this->Wharehouses);

        $this->set(compact('wharehouses'));
    }

    /**
     * View method
     *
     * @param string|null $id Wharehouse id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $wharehouse = $this->Wharehouses->get($id, [
            'contain' => ['Businesses', 'Managers', 'WharehouseTypes', 'Products']
        ]);

        $this->set('wharehouse', $wharehouse);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $wharehouse = $this->Wharehouses->newEntity();
        if ($this->request->is('post')) {
            $wharehouse = $this->Wharehouses->patchEntity($wharehouse, $this->request->getData());
            if ($this->Wharehouses->save($wharehouse)) {
                $this->Flash->success(__('Saved with success.'));

                return $this->redirect(['action' => 'edit',$wharehouse->id]);
            }
            $this->Flash->error(__('Could not be saved. Please, try again.'));
        }
        $businesses = $this->Wharehouses->Businesses->find('list', ['limit' => 200]);
        $managers = $this->Wharehouses->Managers->find('list', ['limit' => 200]);
        $wharehouseTypes = $this->Wharehouses->WharehouseTypes->find('list', ['limit' => 200]);
        $products = $this->Wharehouses->Products->find('list', ['limit' => 200]);
        $this->set(compact('wharehouse', 'businesses', 'managers', 'wharehouseTypes', 'products'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Wharehouse id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $wharehouse = $this->Wharehouses->get($id, [
            'contain' => ['Products']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $wharehouse = $this->Wharehouses->patchEntity($wharehouse, $this->request->getData());
            if ($this->Wharehouses->save($wharehouse)) {
                $this->Flash->success(__('Saved with success.'));

                return $this->redirect(['action' => 'edit',$id]);
            }
            $this->Flash->error(__('Could not be saved. Please, try again.'));
        }
        $businesses = $this->Wharehouses->Businesses->find('list', ['limit' => 200]);
        $managers = $this->Wharehouses->Managers->find('list', ['limit' => 200]);
        $wharehouseTypes = $this->Wharehouses->WharehouseTypes->find('list', ['limit' => 200]);
        $products = $this->Wharehouses->Products->find('list', ['limit' => 200]);
        $this->set(compact('wharehouse', 'businesses', 'managers', 'wharehouseTypes', 'products'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Wharehouse id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $wharehouse = $this->Wharehouses->get($id);
        if ($this->Wharehouses->delete($wharehouse)) {
            $this->Flash->success(__('Deleted with success.'));
        } else {
            $this->Flash->error(__('Could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
    
  
}
