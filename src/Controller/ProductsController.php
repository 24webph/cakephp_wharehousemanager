<?php
namespace WharehouseManager\Controller;

use WharehouseManager\Controller\AppController;

/**
 * Products Controller
 *
 * @property \WharehouseManager\Model\Table\ProductsTable $Products
 *
 * @method \WharehouseManager\Model\Entity\Product[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ProductsController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Suppliers', 'ProductCategories', 'MeasureUnits']
        ];
        $products = $this->paginate($this->Products);

        $this->set(compact('products'));
    }

    /**
     * View method
     *
     * @param string|null $id Product id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $product = $this->Products->get($id, [
            'contain' => ['Suppliers', 'ProductCategories', 'MeasureUnits', 'Wharehouses']
        ]);

        $this->set('product', $product);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $product = $this->Products->newEntity();
        if ($this->request->is('post')) {
            $product = $this->Products->patchEntity($product, $this->request->getData());
            if ($this->Products->save($product)) {
                $this->Flash->success(__('Saved with success.'));

                return $this->redirect(['action' => 'edit',$product->id]);
            }
            $this->Flash->error(__('Could not be saved. Please, try again.'));
        }
        $suppliers = $this->Products->Suppliers->find('list', ['limit' => 200]);
        $productCategories = $this->Products->ProductCategories->find('list', ['limit' => 200]);
        $measureUnits = $this->Products->MeasureUnits->find('list', ['limit' => 200]);
        $wharehouses = $this->Products->Wharehouses->find('list', ['limit' => 200]);
        $this->set(compact('product', 'suppliers', 'productCategories', 'measureUnits', 'wharehouses'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Product id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $product = $this->Products->get($id, [
            'contain' => ['Wharehouses']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $product = $this->Products->patchEntity($product, $this->request->getData());
            if ($this->Products->save($product)) {
                $this->Flash->success(__('Saved with success.'));

                return $this->redirect(['action' => 'edit',$id]);
            }
            $this->Flash->error(__('Could not be saved. Please, try again.'));
        }
        $suppliers = $this->Products->Suppliers->find('list', ['limit' => 200]);
        $productCategories = $this->Products->ProductCategories->find('list', ['limit' => 200]);
        $measureUnits = $this->Products->MeasureUnits->find('list', ['limit' => 200]);
        $wharehouses = $this->Products->Wharehouses->find('list', ['limit' => 200]);
        $this->set(compact('product', 'suppliers', 'productCategories', 'measureUnits', 'wharehouses'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Product id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $product = $this->Products->get($id);
        if ($this->Products->delete($product)) {
            $this->Flash->success(__('Deleted with success.'));
        } else {
            $this->Flash->error(__('Could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
