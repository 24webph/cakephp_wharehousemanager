<?php

namespace WharehouseManager\Controller;

use WharehouseManager\Controller\AppController;
use WharehouseManager\Model\Entity\ProductsWharehouseTransfer;

/**
 * ProductsWharehouses Controller
 *
 * @property \WharehouseManager\Model\Table\ProductsWharehousesTable $ProductsWharehouses
 *
 * @method \WharehouseManager\Model\Entity\ProductsWharehouse[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ProductsWharehousesController extends AppController {

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function _index() {
        $this->paginate = [
            'contain' => ['Products', 'Wharehouses']
        ];
        $productsWharehouses = $this->paginate($this->ProductsWharehouses);

        $this->set(compact('productsWharehouses'));
    }

    /**
     * View method
     *
     * @param string|null $id Products Wharehouse id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function _view($id = null) {
        $productsWharehouse = $this->ProductsWharehouses->get($id, [
            'contain' => ['Products', 'Wharehouses']
        ]);

        $this->set('productsWharehouse', $productsWharehouse);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add(int $wharehouse_id) {
        $productsWharehouse = $this->ProductsWharehouses->newEntity();
        if ($this->request->is('post')) {
            $productsWharehouse = $this->ProductsWharehouses->patchEntity($productsWharehouse, $this->request->getData());
            if ($this->ProductsWharehouses->addStock($productsWharehouse)) {
                $this->Flash->success(__('Saved with success.'));
                return $this->redirect(['controller' => 'wharehouses', 'action' => 'edit', $productsWharehouse->wharehouse_id]);
            }
            $this->Flash->error(__('Could not be saved. Please, try again.'));
        }
        $wharehouse = $this->ProductsWharehouses->Wharehouses->get($wharehouse_id);
        $products = $this->ProductsWharehouses->Products->find('list');
        $wharehouses = $this->ProductsWharehouses->Wharehouses->find('list')->where(['id' => $wharehouse_id]);
        $this->set(compact('productsWharehouse', 'products', 'wharehouses', 'wharehouse'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Products Wharehouse id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null) {
        $productsWharehouse = $this->ProductsWharehouses->get($id, [
            'contain' => ['Wharehouses']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $productsWharehouse = $this->ProductsWharehouses->patchEntity($productsWharehouse, $this->request->getData());
            if ($this->ProductsWharehouses->save($productsWharehouse)) {
                $this->Flash->success(__('Saved with success.'));

                return $this->redirect(['controller' => 'wharehouses', 'action' => 'edit', $productsWharehouse->wharehouse_id]);
            }
            $this->Flash->error(__('Could not be saved. Please, try again.'));
        }
        $products = $this->ProductsWharehouses->Products->find('list')->where(['id' => $productsWharehouse->product_id]);
        $wharehouses = $this->ProductsWharehouses->Wharehouses->find('list')->where(['id' => $productsWharehouse->wharehouse_id]);
        $this->set(compact('productsWharehouse', 'products', 'wharehouses'));
    }
    
     /**
     * AddStock method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function addStock(int $wharehouse_id, int $product_id) {
        $productsWharehouse = $this->ProductsWharehouses->newEntity();
        if ($this->request->is('post')) {
            $productsWharehouse = $this->ProductsWharehouses->patchEntity($productsWharehouse, $this->request->getData());
            if ($this->ProductsWharehouses->addStock($productsWharehouse)) {
                $this->Flash->success(__('Saved with success.'));
                return $this->redirect(['controller' => 'wharehouses', 'action' => 'edit', $productsWharehouse->wharehouse_id]);
            }
            $this->Flash->error(__('Could not be saved. Please, try again.'));
        }
        $wharehouse = $this->ProductsWharehouses->Wharehouses->get($wharehouse_id);
        $products = $this->ProductsWharehouses->Products->find('list')->where(['id' => $product_id]);
        $wharehouses = $this->ProductsWharehouses->Wharehouses->find('list')->where(['id' => $wharehouse_id]);
        $this->set(compact('productsWharehouse', 'products', 'wharehouses', 'wharehouse'));
    }

    /**
     * EditStock method
     *
     * @param string|null $id Products Wharehouse id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function editStock($id = null) {
        $productsWharehouse = $this->ProductsWharehouses->get($id, [
            'contain' => ['Wharehouses']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $productsWharehouse = $this->ProductsWharehouses->patchEntity($productsWharehouse, $this->request->getData());
            if ($this->ProductsWharehouses->editStock($productsWharehouse)) {
                $this->Flash->success(__('Saved with success.'));

                return $this->redirect(['controller' => 'wharehouses', 'action' => 'edit', $productsWharehouse->wharehouse_id]);
            }
            $this->Flash->error(__('Could not be saved. Please, try again.'));
        }
        $products = $this->ProductsWharehouses->Products->find('list')->where(['id' => $productsWharehouse->product_id]);
        $wharehouses = $this->ProductsWharehouses->Wharehouses->find('list')->where(['id' => $productsWharehouse->wharehouse_id]);
        $this->set(compact('productsWharehouse', 'products', 'wharehouses'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Products Wharehouse id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        $productsWharehouse = $this->ProductsWharehouses->get($id);
        if ($this->ProductsWharehouses->delete($productsWharehouse)) {
            $this->Flash->success(__('Deleted with success.'));
        } else {
            $this->Flash->error(__('Could not be deleted. Please, try again.'));
        }

        return $this->redirect(['controller' => 'wharehouses', 'action' => 'edit', $productsWharehouse->wharehouse_id]);
    }

    /**
     * Transfer method
     *
     * @param string|null $id Products Origin Wharehouse id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function transfer($wharehouse_id = null) {
        $productsWharehouse = $this->ProductsWharehouses->newEntity();
        if ($this->request->is('post')) {
            $productsWharehouse = $this->ProductsWharehouses->patchEntity($productsWharehouse, $this->request->getData());
            if ($this->ProductsWharehouses->transfer($productsWharehouse)) {
                $this->Flash->success(__('Transfer done with success.'));
                return $this->redirect(['action' => 'transfer', $productsWharehouse->wharehouse_id]);
            }
            $this->Flash->error(__('Could not be trasfered. Please, try again.'));
        }
        $wharehouse=$this->ProductsWharehouses->Wharehouses->find('all')->where(['id' => $wharehouse_id])->firstOrFail();
        $products = $this->ProductsWharehouses->find('listProductTransfer',['wharehouse_id'=>$wharehouse_id]);
        $wharehouses = $this->ProductsWharehouses->Wharehouses->find('list')->where(['id' => $wharehouse_id]);
        $transferWharehouses = $this->ProductsWharehouses->Wharehouses->find('list')->where(['id !=' => $wharehouse_id]);
        $this->set(compact('productsWharehouse', 'products', 'wharehouses', 'transferWharehouses','wharehouse'));
    }
    
    /**
     * TransferMany method
     *
     * @param string|null $id Products Origin Wharehouse id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function transferMany($wharehouse_id = null) {
        $productsWharehouse = $this->ProductsWharehouses->newEntity();
        if ($this->request->is('post')) {
            $request_data=$this->prepareTransferManyData($this->request->getData());
            $productsWharehouse = $this->ProductsWharehouses->patchEntities($productsWharehouse, $request_data);
            if ($this->ProductsWharehouses->transferMany($productsWharehouse)) {
                $this->Flash->success(__('Transfer done with success.'));
                return $this->redirect(['action' => 'transferMany', $wharehouse_id]);
            }
            $this->Flash->error(__('Could not be trasfered. Please, try again.'));
        }
        
        $wharehouse=$this->ProductsWharehouses->Wharehouses->get($wharehouse_id,['contain' => ['Products']]);
        $products = $this->ProductsWharehouses->find('listProductTransfer',['wharehouse_id'=>$wharehouse_id]);
        $wharehouses = $this->ProductsWharehouses->Wharehouses->find('list')->where(['id' => $wharehouse_id]);
        $transferWharehouses = $this->ProductsWharehouses->Wharehouses->find('list')->where(['id !=' => $wharehouse_id]);
        $this->set(compact('productsWharehouse', 'products', 'wharehouses', 'transferWharehouses','wharehouse'));
    }
    
    private function prepareTransferManyData($data){
       
        $transferProduct=[];
       
        foreach($data['product'] as $transferProductData){
            $transferProductData['transfer_wharehouse_id']=$data['transfer_wharehouse_id'];
            $transferProductData['wharehouse_id']=$data['wharehouse_id'];
            $transferProduct[]=$transferProductData;
        }
        return $transferProduct;
    }

}
