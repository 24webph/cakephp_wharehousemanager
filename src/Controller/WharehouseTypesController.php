<?php
namespace WharehouseManager\Controller;

use WharehouseManager\Controller\AppController;

/**
 * WharehouseTypes Controller
 *
 * @property \WharehouseManager\Model\Table\WharehouseTypesTable $WharehouseTypes
 *
 * @method \WharehouseManager\Model\Entity\WharehouseType[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class WharehouseTypesController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $wharehouseTypes = $this->paginate($this->WharehouseTypes);

        $this->set(compact('wharehouseTypes'));
    }

    /**
     * View method
     *
     * @param string|null $id Wharehouse Type id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $wharehouseType = $this->WharehouseTypes->get($id, [
            'contain' => ['Wharehouses']
        ]);

        $this->set('wharehouseType', $wharehouseType);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $wharehouseType = $this->WharehouseTypes->newEntity();
        if ($this->request->is('post')) {
            $wharehouseType = $this->WharehouseTypes->patchEntity($wharehouseType, $this->request->getData());
            if ($this->WharehouseTypes->save($wharehouseType)) {
                $this->Flash->success(__('Saved with success.'));

                return $this->redirect(['action' => 'edit',$wharehouseType->id]);
            }
            $this->Flash->error(__('Could not be saved. Please, try again.'));
        }
        $this->set(compact('wharehouseType'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Wharehouse Type id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $wharehouseType = $this->WharehouseTypes->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $wharehouseType = $this->WharehouseTypes->patchEntity($wharehouseType, $this->request->getData());
            if ($this->WharehouseTypes->save($wharehouseType)) {
                $this->Flash->success(__('Saved with success.'));

                return $this->redirect(['action' => 'edit',$id]);
            }
            $this->Flash->error(__('Could not be saved. Please, try again.'));
        }
        $this->set(compact('wharehouseType'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Wharehouse Type id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $wharehouseType = $this->WharehouseTypes->get($id);
        if ($this->WharehouseTypes->delete($wharehouseType)) {
            $this->Flash->success(__('Deleted with success.'));
        } else {
            $this->Flash->error(__('Could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
